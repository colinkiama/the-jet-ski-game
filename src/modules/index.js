import game from "./game.js";
import MouseHandler from "./handlers/mouseHandler.js";

const GAME_WIDTH = 360;
const GAME_HEIGHT = 640;

let gameCanvas = document.getElementById("gameCanvas");
gameCanvas.width = GAME_WIDTH;
gameCanvas.height = GAME_HEIGHT;

gameCanvas.onmousemove = MouseHandler.updateMousePosition;
let ctx = gameCanvas.getContext("2d");
var currentGame = new game(ctx, GAME_WIDTH, GAME_HEIGHT);

let lastTime = 0;

// "timestamp gives you a point in time in milliseconds"
function gameLoop(timestamp) {
  // "delta time is the time passed since the last time in milliseconds"
  let deltaTime = timestamp - lastTime;
  lastTime = timestamp;

  ctx.clearRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
  currentGame.start();
  currentGame.update(deltaTime);
  currentGame.draw(ctx);

  requestAnimationFrame(gameLoop);
}

requestAnimationFrame(gameLoop);
