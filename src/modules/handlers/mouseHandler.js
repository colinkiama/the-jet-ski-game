let CurrentMouseX = 0;
let CurrentMouseY = 0;

export { CurrentMouseX, CurrentMouseY };

export default class MouseHandler {
  static updateMousePosition(mouseEventArgs) {
    CurrentMouseX = mouseEventArgs.offsetX;
    CurrentMouseY = mouseEventArgs.offsetY;
  }
}
