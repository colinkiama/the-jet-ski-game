import { CurrentMouseX, CurrentMouseY } from "../handlers/mouseHandler.js";

export class JetSki {
  constructor(gameWidth, gameHeight) {
    // Jetski should be drawn in the middle of the
    // screen
    console.log("Jetski made!");
    this.x = gameWidth / 2;
    this.y = gameHeight / 2;

    this.startX = this.x;
    this.startY = this.y;
  }

  draw(ctx) {
    ctx.fillStyle = "#FFFFFF";
    ctx.beginPath();
    ctx.moveTo(this.x, this.y - 10);
    ctx.lineTo(this.x + 5, this.y + 5);
    ctx.lineTo(this.x - 5, this.y + 5);
    ctx.fill();
  }

  update(deltaTime) {
    this.x = CurrentMouseX;
    this.y = CurrentMouseY;
  }

  reset() {
    this.x = this.startX;
    this.y = this.startY;
  }
}
