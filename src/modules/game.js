import { JetSki } from "./gameObjects/jetSki.js";

const GAMESTATE = {
  PAUSED: 0,
  RUNNING: 1,
  MENU: 2,
  GAMEOVER: 3,
  NEWLEVEL: 4
};

export default class Game {
  constructor(ctx, gameWidth, gameHeight) {
    ctx.fillStyle = "#006994";
    this.gameWidth = gameWidth;
    this.gameHeight = gameHeight;
    this.gameObjects = [];
    this.obstacles = [];
    this.coins = [];

    this.jetSki = new JetSki(this.gameWidth, this.gameHeight);
    this.GameState = GAMESTATE.MENU;
  }

  start() {
    if (this.GameState !== GAMESTATE.MENU) {
      return;
    }

    this.jetSki.reset();
    this.GameState = GAMESTATE.RUNNING;
  }

  update(deltaTime) {
    // TODO: If jetski crashed, game over!
    this.jetSki.update(deltaTime);
    if (
      this.GameState === GAMESTATE.PAUSED ||
      this.GameState === GAMESTATE.MENU ||
      this.GameState === GAMESTATE.GAMEOVER
    )
      return;
  }

  draw(ctx) {
    ctx.fillStyle = "#006994";
    ctx.fillRect(0, 0, this.gameWidth, this.gameHeight);

    this.jetSki.draw(ctx);

    switch (this.GameState) {
      case GAMESTATE.PAUSED:
        break;
      case GAMESTATE.MENU:
        break;
      case GAMESTATE.GAMEOVER:
        break;
      default:
        break;
    }
  }
}
